## Moduli

+ Main, gestisce il cambio fase e porta allo stato iniziale le domande saltate
    e i OAV dei quartieri.
+ Question, permette di registrare le risposte verificando i valori forniti.
+ Constraint, modifica richiesta in base alle risposte ricevute.
+ CFgeneration, genera OAV con i CF in base ai vincoli in richiesta.
+ Rule, assegna i CF agli appartamenti.
+ Proposal, fornisce in output gli appartamenti con CF più alto.

+ Domain, contiene definizione per i template di richiesta, cliente,
    appartamento, città e quartiere.
+ OAV, contiene definizione del template OAV e le regole per le sue
    combinazioni.

Per eseguire il codice è sufficiente caricare start.bat e avviare l'esecuzione:

```
$ clips -f start.bat
CLIPS> (run)
```

Il sistema inizierà a porre le domande chiedendo prima la tipologia del vincolo:

1. Non negoziabile, hard.
2. Importante, soft.
3. Indifferente, dc non verra più proposta.
4. Salta, verrà riproposta in seguito.
5. Esci, finisce la fase di domande e passa alla successiva.

Se non si risponde ad alcuna domanda il sistema non proporrà nulla.
Alcune domande come quelle sui singoli servizi richiedono solamente la 
tipologia del vincolo considerando che se si è risposto si desidera
tale servizio.

Le domande su dati personali del cliente non sono influenzate se si è
deciso una tipologia di vincolo hard o soft.
