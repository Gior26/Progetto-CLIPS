;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule DOMAIN (export ?ALL))
(deftemplate appartamento
  (slot id (type SYMBOL) (default-dynamic (gensym*)))
  (multislot CF (default-dynamic (create$)))
  (multislot extra (default-dynamic (create$)))
  (slot mq (type INTEGER) (range 0 ?VARIABLE))
  (slot nstanze (type INTEGER))
  (slot nservizi (type INTEGER))
  (slot piano (type INTEGER) (range -20 500) (default -20))
  (slot citta (type SYMBOL))
  (slot zona (allowed-symbols noanswer centro prima_cintura periferia)
   (default noanswer))
  (slot quartiere (type SYMBOL))
  (slot box_auto (type INTEGER) (range -1 ?VARIABLE) (default -1))
  (slot terrazzo (type SYMBOL) (allowed-symbols unk si no) (default unk))
  (slot prezzo (type INTEGER) (range 1 ?VARIABLE) (default ?NONE))
)

(deftemplate cliente
  (slot id (type SYMBOL) (default-dynamic (gensym*)))
  (slot eta (type INTEGER) (range 17 ?VARIABLE))
  (slot figli (type INTEGER) (range 0 15))
  (slot sportivo (type SYMBOL) (allowed-symbols si no) (default no))
  (slot studente (type SYMBOL) (allowed-symbols si no) (default no))
  (slot lavoratore (type SYMBOL) (allowed-symbols si no) (default no))
  (slot pendolare (type SYMBOL) (allowed-symbols si no) (default no))
  (slot mezzi (type SYMBOL) (allowed-symbols si no) (default no))
)

(deftemplate richiesta
  (slot id (type SYMBOL) (default-dynamic (gensym*)))
  (slot persone (type INTEGER) (range 0 ?VARIABLE))
  ; budget cliente == (appartamento (id appartamento) (prezzo ?budget))
  ; (cliente (id)
  (slot cliente (default ?NONE))
  ; (appartamento (id)
  (slot appartamento (default ?NONE))
  (multislot servizi)
  ; slot per tipologia vincolo
  ; dc:   don't care
  ; soft: negoziabile
  ; hard: non negoziabile
  (slot vincolo-mq (allowed-symbols unk hard soft dc))
  (slot vincolo-nstanze (allowed-symbols unk hard soft dc))
  (slot vincolo-nservizi (allowed-symbols unk hard soft dc))
  (slot vincolo-piano (allowed-symbols unk hard soft dc))
  (slot vincolo-citta (allowed-symbols unk hard soft dc))
  (slot vincolo-zona (allowed-symbols unk hard soft dc))
  (slot vincolo-quartiere (allowed-symbols unk hard soft dc))
  (slot vincolo-box_auto (allowed-symbols unk hard soft dc))
  (slot vincolo-terrazzo (allowed-symbols unk hard soft dc))
  (slot vincolo-prezzo (allowed-symbols unk hard soft dc))
)

(deftemplate citta
  (slot nome (type SYMBOL))
  (multislot quartieri (cardinality 1 ?VARIABLE))
)

(deftemplate quartiere
  (slot nome (type SYMBOL))
  (multislot servizi)
)

(deffacts appartamenti
 (appartamento (mq 100) (nstanze 5) (nservizi 2)
  (piano 0) (citta Torino) (zona centro)
  (quartiere qto1) (terrazzo no) (box_auto 0)
  (extra condizionatore) (prezzo 250000))
 (appartamento (mq 90) (nstanze 3) (nservizi 1)
  (piano 2) (citta Torino) (zona periferia)
  (quartiere qto2) (terrazzo si) (box_auto 2)
  (extra ascensore) (prezzo 150000))
 (appartamento (mq 200) (nstanze 10) (nservizi 3)
  (piano 0) (citta Torino) (zona prima_cintura)
  (quartiere qto3) (terrazzo si) (box_auto 3)
  (extra piscina) (prezzo 250000))
 (appartamento (mq 50) (nstanze 3) (nservizi 1)
  (piano 4) (citta Milano) (zona centro)
  (quartiere qmi1) (terrazzo si) (box_auto 0)
  (extra ascensore) (prezzo 350000)))

(deffacts elenco-citta
 (citta (nome Torino) (quartieri qto1 qto2 qto3))
 (citta (nome Milano) (quartieri qmi1)))

(deffacts elenco-quartieri
 (quartiere (nome qto1) (servizi metro autobus palestra scuola))
 (quartiere (nome qto2) (servizi treno asilo scuola))
 (quartiere (nome qto3) (servizi universita palestra))
 (quartiere (nome qmi1)
  (servizi treno palestra scuola asilo piscina)))

(deffacts richieste
 (appartamento (id app1) (prezzo 1))
 (cliente (id cl1))
 (richiesta (id gen1)
  (cliente cl1)
  (appartamento app1))
)
