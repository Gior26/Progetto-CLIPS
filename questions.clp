;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule QUESTIONS (export deftemplate domanda)
 (import DOMAIN ?ALL)
 (import OAV ?ALL)
)
(deftemplate domanda
 (multislot attributo (default ?NONE))
 (slot testo (type STRING))
 (multislot valori-accettati (default ?NONE))
 (slot gia-chiesta (default FALSE))
 (slot saltata (default FALSE))
 (multislot prerequisiti (default-dynamic (create$))))

(deffunction poni-domanda (?domanda ?accettati)
 (printout t ?domanda)
 (bind ?scelta (nth$ 1 ?accettati))
 (switch ?scelta
  (case POSITIVO then
   (bind ?risposta (read))
   (while (or (not (integerp ?risposta))
           (<= ?risposta 0)) do
    (printout t ?domanda)
    (bind ?risposta (read))))
  (case ETA then
   (bind ?risposta (read))
   (while (or (not (integerp ?risposta))
           (<= ?risposta 17)) do
    (printout t ?domanda)
    (bind ?risposta (read))))
  (case INTERO then
   (bind ?risposta (read))
   (while (not (integerp ?risposta)) do
    (printout t ?domanda)
    (bind ?risposta (read))))
  (case CITTA then
   (bind ?risposta (read))
   (while (not (lexemep ?risposta)) do
    (printout t ?domanda)
    (bind ?risposta (read))))
  (default then
   (foreach ?val ?accettati (printout t ?val" "))
   (printout t "]:"crlf)
   (bind ?risposta (read))
   (if (lexemep ?risposta)
    then (bind ?risposta (lowcase ?risposta)))
   (while (not (member$ ?risposta ?accettati)) do
    (printout t ?domanda)
    (foreach ?val ?accettati (printout t ?val" "))
    (printout t "]:"crlf)
    (bind ?risposta (read))
    (if (lexemep ?risposta)
     then (bind ?risposta (lowcase ?risposta)))))
 ) ?risposta)

(deffunction tipologia-vincolo (?testo)
 (bind ?risposta -1)
 (while (or
         (not(integerp ?risposta))
         (< ?risposta 1) (> ?risposta 5))
  (printout t ?testo crlf)
  (printout t "Questa domanda è:" crlf)
  (printout t "1)Non negoziabile" crlf)
  (printout t "2)Importante" crlf)
  (printout t "3)Indifferente" crlf)
  (printout t "4)Salta" crlf)
  (printout t "5)Esci" crlf)
  (bind ?risposta (read))) ?risposta)

(deffunction verifica-interruzione (?risposta ?cont ?d ?r)
 (if (< ?risposta 4) then
  (modify ?d (gia-chiesta TRUE)))
 (if (= ?risposta 4) then
  (modify ?d (saltata TRUE)))
 (retract ?cont)
 (if (= ?risposta 5) then
  (modify ?r (id gen1))(assert (continua EXIT)) else
  (assert (continua TRUE))
 )
)

(defrule risolvi-prerequisiti (declare (salience 1000))
  ?d <- (domanda (gia-chiesta FALSE)
      (prerequisiti $?pre&:(> (length$ ?pre) 0)))
  (domanda (attributo $?prima ?vincolo&:(member$ ?vincolo ?pre) $?dopo)
   (gia-chiesta TRUE))
  =>
  (modify ?d (prerequisiti (rest$ ?pre)))
)

(defrule fine-domande (declare (salience 500))
  ?cont <- (continua EXIT)
  =>
  (printout t "Elaboro risposte" crlf)
  (retract ?cont)
  (assert (continua TRUE))
  (return))

(defrule poni-una-domanda
 ?cont <- (continua TRUE)
 ?r <- (richiesta (id ?id) (appartamento ?app) (cliente ?cliente))
 ?c <- (cliente (id ?cliente))
 ?a <- (appartamento (id ?app))
 ?d <- (domanda (gia-chiesta FALSE) (saltata FALSE) (testo ?testo)
     (attributo ? ?attributo) (valori-accettati $?accettati)
     (prerequisiti))
 =>
 (bind ?risposta (tipologia-vincolo ?testo))
 (switch ?attributo
  (case mq then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (mq (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-mq hard)))
    (case 2 then (modify ?r (vincolo-mq soft)))
    (case 3 then (modify ?r (vincolo-mq dc)))
   )
  )
  (case nservizi then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (nservizi (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-nservizi hard)))
    (case 2 then (modify ?r (vincolo-nservizi soft)))
    (case 3 then (modify ?r (vincolo-nservizi dc)))
   )
  )
  (case nstanze then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (nstanze (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-nstanze hard)))
    (case 2 then (modify ?r (vincolo-nstanze soft)))
    (case 3 then (modify ?r (vincolo-nstanze dc)))
   )
  )
  (case citta then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (citta (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-citta hard)))
    (case 2 then (modify ?r (vincolo-citta soft)))
    (case 3 then (modify ?r (vincolo-citta dc)))
   )
  )
  (case zona then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (zona (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-zona hard)))
    (case 2 then (modify ?r (vincolo-zona soft)))
    (case 3 then (modify ?r (vincolo-zona dc)))
   )
  )
  (case box_auto then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (box_auto (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-box_auto hard)))
    (case 2 then (modify ?r (vincolo-box_auto soft)))
    (case 3 then (modify ?r (vincolo-box_auto dc)))
   )
  )
  (case terrazzo then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (terrazzo (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-terrazzo hard)))
    (case 2 then (modify ?r (vincolo-terrazzo soft)))
    (case 3 then (modify ?r (vincolo-terrazzo dc)))
   )
  )
  (case prezzo then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (prezzo (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-prezzo hard)))
    (case 2 then (modify ?r (vincolo-prezzo soft)))
    (case 3 then (modify ?r (vincolo-prezzo dc)))
   )
  )
  (case piano then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?a (piano (poni-domanda ?testo ?accettati))))
   (switch ?risposta
    (case 1 then (modify ?r (vincolo-piano hard)))
    (case 2 then (modify ?r (vincolo-piano soft)))
    (case 3 then (modify ?r (vincolo-piano dc)))
   )
  )
  (case metro then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case treno then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case autobus then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case scuola then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case asilo then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case palestra then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case piscina then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case universita then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute servizio ?attributo) (value si) (CF 0.2))))
   )
  )
  (case ascensore then
   (switch ?risposta
    (case 1 then (assert (oav (object appartamento)
                          (attribute extra ?attributo) (value si) (CF 0.9))))
    (case 2 then (assert (oav (object appartamento)
                          (attribute extra ?attributo) (value si) (CF 0.2))))
   )
  )
  (case persone then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?r (persone (poni-domanda ?testo ?accettati))))
  )
  (case figli then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (figli (poni-domanda ?testo ?accettati))))
  )
  (case studente then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (studente (poni-domanda ?testo ?accettati))))
  )
  (case sportivo then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (sportivo (poni-domanda ?testo ?accettati))))
  )
  (case pendolare then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (pendolare (poni-domanda ?testo ?accettati))))
  )
  (case mezzi then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (mezzi (poni-domanda ?testo ?accettati))))
  )
  (case eta then
   (if (or(= ?risposta 1) (= ?risposta 2)) then
    (modify ?c (eta (poni-domanda ?testo ?accettati))))
  )
  )
 (verifica-interruzione ?risposta ?cont ?d ?r)
)

(defrule poni-una-domanda-quartiere (declare (salience 100))
 ?cont <- (continua TRUE)
 ?r <- (richiesta (id ?id) (appartamento ?app))
 ?a <- (appartamento (id ?app) (citta ?citta))
 (citta (nome ?citta) (quartieri $?accettati&:(> (length$ $?accettati) 0)))
 ?d <- (domanda (gia-chiesta FALSE) (saltata FALSE)
     (testo ?testo)
     (attributo appartamento quartiere)
     (prerequisiti))
 =>
 (bind ?risposta (tipologia-vincolo ?testo))
 (if (or(= ?risposta 1) (= ?risposta 2)) then
  (modify ?a (quartiere (poni-domanda ?testo ?accettati))))
    (switch ?risposta
     (case 1 then (modify ?r (vincolo-quartiere hard)))
     (case 2 then (modify ?r (vincolo-quartiere soft)))
     (case 3 then (modify ?r (vincolo-quartiere dc)))
    )
 (verifica-interruzione ?risposta ?cont ?d ?r)
)

;; Per le domande con valori-accettati INTERO nella regola input
;; si verificherà con integerp e che il valore sia >= 0.
;; Per le domande con valori-accettati POSIVO nella regola input
;; si verificherà con integerp e che il valore sia > 0.
;; Per le domande con valori-accettati QUARTIERE nella regola input
;; (domanda (attributo citta) (gia-chiesta TRUE))
;; e i valori-accettati sono quelli di ?q:
;; (appartamento (id richiesta-corrente) (citta ?c))
;; (citta (quartieri ?q))
(deffacts domande
 (domanda (attributo appartamento nservizi)
  (testo "Quanti bagni dovrebbe avere la nuova casa? [1,2,...]:")
  (valori-accettati POSITIVO))

 (domanda (attributo appartamento nstanze)
  (testo "Quanti stanze dovrebbe avere la nuova casa? [1,2,...]:")
  (valori-accettati POSITIVO))

 (domanda (attributo appartamento zona)
  (prerequisiti citta)
  (testo "In quale zona della città vorresti casa? [")
  (valori-accettati centro periferia prima_cintura))

 (domanda (attributo appartamento quartiere)
  (prerequisiti citta zona)
  (testo "In che quartiere vorresti abitare? [")
  (valori-accettati QUARTIERE))

 (domanda (attributo appartamento terrazzo)
  (testo "Vuole un terrazzo? [")
  (valori-accettati si no))

 (domanda (attributo appartamento metro)
  (testo "Vuole la metro nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento treno)
  (testo "Vuole la stazione dei treni [")
  (valori-accettati si no))

 (domanda (attributo appartamento autobus)
  (testo "Vuole gli autobus nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento scuola)
  (testo "Vuole la scuola nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento asilo)
  (testo "Vuole l'asilo nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento universita)
  (testo "Vuole l'università nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento palestra)
  (testo "Vuole la palestra nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento piscina)
  (testo "Vuole una piscina comunale nel quartiere? [")
  (valori-accettati si no))

 (domanda (attributo appartamento ascensore)
  (testo "Vuole l'ascensore? ")
  (valori-accettati si no))

 (domanda (attributo appartamento piano)
  (testo "A quale piano preferiresti avere casa? [0,1,2,...]:")
  (valori-accettati INTERO))

 (domanda (attributo appartamento mq)
  (testo "Quanti mq dovrebbe avere la nuova casa? [1,2,...]:")
  (valori-accettati POSITIVO))

 (domanda (attributo appartamento persone)
  (testo "Quante persone vivrebbero nella nuova casa? [1,2,...]:")
  (valori-accettati POSITIVO))

 (domanda (attributo cliente figli)
  (prerequisiti persone)
  (testo "Ha dei figli? [0,1,2,...]:")
  (valori-accettati INTERO))

 (domanda (attributo appartamento box_auto)
  (testo "Quanti box auto vorrebbe avere? [0,1,...]:")
  (valori-accettati INTERO))

 (domanda (attributo cliente eta)
  (testo "Quanti anni ha? [18,19,...")
  (valori-accettati ETA))

 (domanda (attributo cliente mezzi)
  (testo "Usa i mezzi pubblici? [")
  (valori-accettati si no))

 (domanda (attributo cliente pendolare)
  (prerequisiti studente lavoratore)
  (testo "È un pendolare? [")
  (valori-accettati si no))

 (domanda (attributo cliente sportivo)
  (testo "È una persona sportiva? [")
  (valori-accettati si no))

 (domanda (attributo cliente studente)
  (testo "Attualmente studia? [")
  (valori-accettati si no))

 (domanda (attributo appartamento prezzo)
  (testo "Qual'è il suo budget? [1...N]:")
  (valori-accettati POSITIVO))

 (domanda (attributo appartamento citta)
  (testo "In che città vorresti abitare?:")
  (valori-accettati CITTA))

 (continua TRUE)
)
