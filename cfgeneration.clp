;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule CFGENERATION
 (import OAV deftemplate oav)
 (import DOMAIN deftemplate ?ALL)
)

(deffunction calcolo-mq-soft (?desiderata)
  (bind ?offset (/ ?desiderata 10))
  (bind ?pos_dec 0.225)
  (bind ?neg_dec 0.45)
  (loop-for-count (?i 4)
   (bind ?pos (+ ?desiderata (* ?i (* ?offset 2))))
   (bind ?neg (- ?desiderata (* ?i (* ?offset 2))))
   (assert
    (oav (object appartamento)
     (attribute mq)
     (value (- ?pos ?offset)
      (+ ?pos ?offset))
     (CF (- 0.9 (* ?i ?pos_dec))))
    (oav (object appartamento)
     (attribute mq)
     (value (- ?neg ?offset)
      (+ ?neg ?offset))
     (CF (- 0.9 (* ?i ?neg_dec))))
   )
  )
  (assert
   (oav (object appartamento) (attribute mq)
    (value (- ?desiderata ?offset) (+ ?desiderata ?offset))
    (CF 0.9))
   (oav (object appartamento) (attribute mq)
    (value 0 (- ?desiderata (* 9 ?offset)))
    (CF -0.9))
   (oav (object appartamento) (attribute mq)
    (value (+ ?desiderata (* 9 ?offset)) 999999999)
    (CF -0.9))
  )
)

(deffunction calcolo-prezzo-soft (?desiderata)
  =>
  (bind ?offset (/ ?desiderata 20))
  (loop-for-count (?i 4)
   (bind ?val1 (+ ?desiderata (* ?i (* ?offset 2))))
   (assert
    (oav (object appartamento)
     (attribute prezzo)
     (value (- ?val1 ?offset)
      (+ ?val1 ?offset))
     (CF (- 0.9 (* ?i 0.45))))))
  (assert
   (oav (object appartamento) (attribute prezzo)
    (value (- ?desiderata ?offset) (+ ?desiderata ?offset))
    (CF 0.9))
   (oav (object appartamento) (attribute prezzo)
    (value 0 (- ?desiderata ?offset))
    (CF 0.9))
   (oav (object appartamento) (attribute prezzo)
    (value (+ ?desiderata (* 9 ?offset)) 999999999)
    (CF -0.9))
  )
)

(deffunction calcolo-nstanze-soft (?desiderata)
  (bind ?offset 1)
  (bind ?pos_dec 0.225)
  (bind ?neg_dec 0.45)
  (loop-for-count (?i 4)
   (bind ?pos (+ ?desiderata (* ?i (* ?offset 2))))
   (bind ?neg (- ?desiderata (* ?i (* ?offset 2))))
   (assert
    (oav (object appartamento)
     (attribute nstanze)
     (value (- ?pos ?offset)
      (+ ?pos ?offset))
     (CF (- 0.9 (* ?i ?pos_dec))))
    (oav (object appartamento)
     (attribute nstanze)
     (value (- ?neg ?offset)
      (+ ?neg ?offset))
     (CF (- 0.9 (* ?i ?neg_dec))))
   )
  )
  (assert
   (oav (object appartamento) (attribute nstanze)
    (value (- ?desiderata ?offset) (+ ?desiderata ?offset))
    (CF 0.9))
   (oav (object appartamento) (attribute nstanze)
    (value (+ ?desiderata (* 9 ?offset)) 100)
    (CF -0.9))
  )
)

;; SERVIZI IN QUARTIERE
(defrule quartiere-servizi
 (richiesta (appartamento ?app)
  (servizi $?servizi&:(> (length$ $?servizi) 0)))
 (quartiere (nome ?nome) (servizi $?qservizi))
 (not (oav (object quartiere)(attribute servizi)(value ?nome)))
 =>
 (bind ?max (length$ $?servizi))
 (bind ?count 0)
 (foreach ?s $?servizi
  (if (member$ ?s $?qservizi)
   then (bind ?count (+ ?count 1))))
 (bind ?CF (* (/ ?count ?max) 0.9))
 (if (> ?CF 0.0) then
 (assert (oav (object quartiere)
          (attribute servizi)
          (value ?nome)
          (CF ?CF)))))

;; EXTRA APPARTAMENTO
(defrule extra-appartamento
 (richiesta (appartamento ?app))
 (appartamento (id ?app) (extra $?desiderata&:(> (length$ $?desiderata) 0)))
 (appartamento (id ?id&:(neq ?app ?id)) (extra $?extra))
 (not (oav (object appartamento)(attribute extra)(value ?id)))
 =>
 (bind ?max (length$ $?desiderata))
 (bind ?count 0)
 (foreach ?c $?desiderata
  (if (member$ ?c $?extra)
   then (bind ?count (+ ?count 1))))
 (bind ?CF (* (/ ?count ?max) 0.9))
 (assert (oav (object appartamento)
          (attribute extra)
          (value ?id)
          (CF ?CF))))

;; BOX AUTO
(defrule box_auto-desiderati-1
 (richiesta (appartamento ?app) (vincolo-box_auto hard|soft))
 (appartamento (id ?app) (box_auto ?desiderata&:(> ?desiderata -1)))
 (appartamento (box_auto ?box&:(>= ?box ?desiderata)))
 =>
 (assert (oav (object appartamento) (attribute box_auto)
          (value ?box) (CF 0.9))))

(defrule box_auto-desiderati-2soft
 (richiesta (appartamento ?app) (vincolo-box_auto soft))
 (appartamento (id ?app) (box_auto ?nbox&:(> ?nbox -1)))
 (appartamento (box_auto ?cbox&:(< ?cbox ?nbox)))
 =>
 (bind ?CF (/ ?cbox ?nbox))
 (assert (oav (object appartamento) (attribute box_auto)
          (value ?cbox) (CF ?CF))))

(defrule box_auto-desiderati-2hard
 (richiesta (appartamento ?app) (vincolo-box_auto hard))
 (appartamento (id ?app) (box_auto ?desiderata&:(> ?desiderata -1)))
 (appartamento (box_auto ?box&:(<> ?box ?desiderata)))
 =>
 (assert (oav (object appartamento) (attribute box_auto)
          (value ?box) (CF -0.9))))
;; CITTA
(defrule citta-desiderata
 (richiesta (appartamento ?app) (vincolo-citta hard|soft))
 (appartamento (id ?app) (citta ?desiderata&:(neq ?desiderata nil)))
 =>
 (assert (oav (object citta)
          (attribute nome)
          (value ?desiderata)
          (CF 0.9))))

(defrule citta-non-desiderata
 (richiesta (appartamento ?app1) (vincolo-citta ?vincolo))
 (appartamento (id ?app1)
  (citta ?desiderata&:(neq ?desiderata nil)))
 (citta (nome ?nondesiderata&:(neq ?desiderata ?nondesiderata)))
 =>
 (if (eq ?vincolo hard)
  then
  (bind ?CF -1.0)
  else
  (bind ?CF 0.0))
 (assert (oav (object citta)
          (attribute nome)
          (value ?nondesiderata)
          (CF ?CF))))

;; QUARTIERE
(defrule quartiere-desiderata
 (richiesta (appartamento ?app) (vincolo-quartiere hard|soft))
 (appartamento (id ?app) (quartiere ?desiderata))
 =>
 (assert (oav (object quartiere)
          (attribute nome)
          (value ?desiderata)
          (CF 0.9))))

(defrule quartiere-non-desiderata
 (richiesta (appartamento ?app1)
  (vincolo-quartiere ?vincolo & hard|soft))
 (appartamento (id ?app1) (quartiere ?desiderata))
 (quartiere (nome ?nondesiderata&:(neq ?desiderata ?nondesiderata)))
 =>
 (if (eq ?vincolo hard)
  then
  (bind ?CF -1.0)
  else
  (bind ?CF 0.0))
 (assert (oav (object quartiere)
          (attribute nome)
          (value ?nondesiderata)
          (CF ?CF))))

;; ZONA
(defrule zona-desiderata
 (richiesta (appartamento ?app) (vincolo-zona hard|soft))
 (appartamento (id ?app) (zona ?desiderata&:(neq ?desiderata noanswer)))
 =>
 (assert (oav (object appartamento)
          (attribute zona)
          (value ?desiderata) (CF 0.9))))

(defrule zona-non-desiderata
 (richiesta (appartamento ?app) (vincolo-zona ?vincolo))
 (appartamento (id ?app) (zona ?desiderata&:(neq ?desiderata noanswer)))
 (appartamento (zona ?zona&:(neq ?desiderata ?zona)))
 =>
 (if (eq ?vincolo hard)
  then
  (bind ?CF -1.0)
  else
  (bind ?CF 0.2))
 (assert (oav (object appartamento)
          (attribute zona)
          (value ?zona) (CF ?CF))))

;; MQ
(defrule metratura-desiderata-soft
  (richiesta (appartamento ?app) (vincolo-mq soft))
  (appartamento (id ?app) (mq ?desiderata&:(> ?desiderata 0)))
  =>
  (calcolo-mq-soft ?desiderata)
)

(defrule metratura-desiderata-hard
  (richiesta (appartamento ?app) (vincolo-mq hard))
  (appartamento (id ?app) (mq ?desiderata&:(> ?desiderata 0)))
  =>
  (assert
   (oav (object appartamento) (attribute mq)
    (value ?desiderata (+ ?desiderata 1))
    (CF 0.9))
   (oav (object appartamento) (attribute mq)
    (value 0 ?desiderata)
    (CF -0.9))
   (oav (object appartamento) (attribute mq)
    (value (+ ?desiderata 1) 999999999)
    (CF -0.9))
  )
)

;; PREZZO
(defrule prezzo-desiderato-soft
  (richiesta (appartamento ?app) (vincolo-prezzo soft))
  (appartamento (id ?app) (prezzo ?desiderata&:(<> ?desiderata 1)))
  =>
  (calcolo-prezzo-soft ?desiderata)
)
(defrule prezzo-desiderato-hard
  (richiesta (appartamento ?app) (vincolo-prezzo hard))
  (appartamento (id ?app) (prezzo ?desiderata&:(<> ?desiderata 1)))
  =>
  (assert
   (oav (object appartamento) (attribute prezzo)
    (value ?desiderata (+ ?desiderata 1))
    (CF 0.9))
   (oav (object appartamento) (attribute prezzo)
    (value 0 ?desiderata)
    (CF 0.9))
   (oav (object appartamento) (attribute prezzo)
    (value (+ ?desiderata 1) 999999999)
    (CF -0.9))
  )
)

;; NUMERO STANZE
(defrule npersone-stanze
  (richiesta (appartamento ?app) (vincolo-nstanze dc|unk)
   (persone ?p&:(> ?p 1)))
  =>
  (bind ?min (+ ?p 1))
  (assert
   (oav (object appartamento) (attribute nstanze)
    (value (- ?min 1) (+ ?min 1)) (CF 0.45))
   (oav (object appartamento) (attribute nstanze)
    (value 0 (- ?min 1)) (CF -0.1))
   (oav (object appartamento) (attribute nstanze)
    (value (+ ?min 1) 100) (CF 0.9))
  )
)
(defrule nstanze-hard
  (richiesta (appartamento ?app) (vincolo-nstanze hard))
  (appartamento (id ?app) (nstanze ?n))
  =>
  (assert
   (oav (object appartamento)(attribute nstanze) (value ?n 100) (CF -0.9))
   (oav (object appartamento) (attribute nstanze) (value (- ?n 1) ?n) (CF 0.9))
   (oav (object appartamento) (attribute nstanze) (value 1 (- ?n 1)) (CF -0.9)))
)

(defrule nstanze-soft
  (richiesta (appartamento ?app) (vincolo-nstanze soft))
  (appartamento (id ?app) (nstanze ?n))
  =>
  (calcolo-nstanze-soft ?n)
)

;; NSERVIZI
(defrule nservizi-desiderati
  (richiesta (appartamento ?app) (vincolo-nservizi soft|hard))
  (appartamento (id ?app) (nservizi ?n))
  =>
  (assert (oav (object appartamento)(attribute nservizi) (value ?n) (CF 0.9)))
)
(defrule nservizi-hard
  (richiesta (appartamento ?app) (vincolo-nservizi hard))
  (appartamento (id ?app) (nservizi ?nd))
  (appartamento (nservizi ?n&:(<> ?n ?nd)))
  =>
  (assert (oav (object appartamento)(attribute nservizi) (value ?n) (CF -0.9)))
)

(defrule nservizi-soft
  (richiesta (appartamento ?app) (vincolo-nservizi soft))
  (appartamento (id ?app) (nservizi ?nd))
  (appartamento (nservizi ?n&:(<> ?n ?nd)))
  =>
  (bind ?CF (- 0.9 (* 0.45 (abs (- ?nd ?n)))))
  (if (< ?CF -0.9) then (bind ?CF -0.9))
  (assert (oav (object appartamento)(attribute nservizi) (value ?n) (CF ?CF)))
)

;; PIANO
(defrule piano-diverso
  (richiesta (appartamento ?app) (vincolo-piano ?vincolo))
  (appartamento (id ?app)(piano ?dp&:(> ?dp -20)))
  (appartamento (piano ?cp&:(<> ?dp ?cp)))
  (not (oav (object appartamento)(attribute piano)(value ?cp)))
  =>
  (bind ?CF (- 0.9(* 0.4 (abs (- ?dp ?cp)))))
  (if (or(< ?CF -0.9)(eq ?vincolo hard)) then (bind ?CF -0.9))
  (assert (oav (object appartamento)(attribute piano)(value ?cp) (CF ?CF)))
)
(defrule piano-desiderato
  (richiesta (appartamento ?app))
  (appartamento (id ?app)(piano ?dp&:(> ?dp -20)))
  (appartamento (piano ?dp))
  (not (oav (object appartamento)(attribute piano)(value ?dp)))
  =>
  (assert (oav (object appartamento)(attribute piano)(value ?dp) (CF 0.9)))
)

;; TERRAZZO
(defrule terrazzo-desiderato
 (richiesta (appartamento ?app) (vincolo-terrazzo hard|soft))
 (appartamento (id ?app) (terrazzo ?desiderata))
 =>
 (assert (oav (object appartamento)
          (attribute terrazzo)
          (value ?desiderata)
          (CF 0.9))))

(defrule terrazzo-non-desiderato
 (richiesta (appartamento ?app1)
  (vincolo-terrazzo ?vincolo & hard|soft))
 (appartamento (id ?app1) (terrazzo ?desiderata))
 (appartamento (terrazzo ?nondesiderata&:(neq ?desiderata ?nondesiderata)))
 =>
 (if (eq ?vincolo hard)
  then
  (bind ?CF -0.9)
  else
  (bind ?CF 0.0))
 (assert (oav (object appartamento)
          (attribute terrazzo)
          (value ?nondesiderata)
          (CF ?CF))))
