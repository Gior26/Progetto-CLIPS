;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule RULES
 (import OAV deftemplate oav)
 (import DOMAIN deftemplate ?ALL)
)

;; PIANO
(defrule assegna-CF-piano
 (richiesta (appartamento ?app) (vincolo-piano ?vpiano & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (piano ?piano) (CF $?CF))
 (oav (object appartamento) (attribute piano)
  (value ?piano)
  (CF ?CFpiano&~:(member$ ?CFpiano $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFpiano))))

;; MQ
(defrule assegna-CF-mq
 (richiesta (appartamento ?app) (vincolo-mq ?vmq & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (mq ?mq) (CF $?CF))
 (oav (object appartamento) (attribute mq)
  (value ?mq1X&:(<= ?mq1X ?mq) ?mq2X&:(< ?mq ?mq2X))
  (CF ?CFmq&~:(member$ ?CFmq $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFmq))))

;; PREZZO
(defrule assegna-CF-prezzo
 (richiesta (appartamento ?app) (vincolo-prezzo ?vprezzo & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (prezzo ?prezzo) (CF $?CF))
 (oav (object appartamento) (attribute prezzo)
  (value ?p1&:(<= ?p1 ?prezzo) ?p2&:(< ?prezzo ?p2))
  (CF ?CFprezzo&~:(member$ ?CFprezzo $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFprezzo))))

;; CITTA
(defrule assegna-CF-citta
 (richiesta (appartamento ?app) (vincolo-citta ?vcitta & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (citta ?citta) (CF $?CF))
 (oav (object citta) (attribute nome) (value ?citta)
  (CF ?CFcitta&~:(member$ ?CFcitta $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFcitta))))

;; ZONA
(defrule assegna-CF-zona
 (richiesta (appartamento ?app) (vincolo-zona ?vzona & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (zona ?zona) (CF $?CF))
 (oav (object appartamento) (attribute zona) (value ?zona)
  (CF ?CFzona&~:(member$ ?CFzona $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFzona))))


;; QUARTIERE
(defrule solo-servizi (declare (salience 100))
  ?s <- (oav (object quartiere) (attribute servizi) (value ?nome) (CF ?CF1))
  (not (oav (object quartiere) (attribute nome) (value ?nome)))
  =>
  (retract ?s)
  (assert (oav (object quartiere) (attribute nome) (value ?nome)
           (CF ?CF1))))

(defrule servizi-in-quartiere (declare (salience 100))
  ?s <- (oav (object quartiere) (attribute servizi) (value ?nome) (CF ?CF1))
  ?q <- (oav (object quartiere) (attribute nome) (value ?nome) (CF ?CF2))
  =>
  (retract ?s ?q)
  (if (<= ?CF2 -0.9) then (bind ?CF1 ?CF2))
  (assert (oav (object quartiere) (attribute nome) (value ?nome)
           (CF (max ?CF1 ?CF2)))))

(defrule assegna-CF-quartiere
 (richiesta (appartamento ?app) (vincolo-quartiere ?vincolo & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (quartiere ?qua) (CF $?CF))
 (oav (object quartiere) (attribute nome) (value ?qua)
  (CF ?CFqua&~:(member$ ?CFqua $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFqua))))

(defrule assegna-CF-quartiere-servizi
  (richiesta (appartamento ?app)(servizi $?servizi&:(> (length$ $?servizi) 0)))
  ?a <- (appartamento (id ?id&:(neq ?id ?app)) (quartiere ?qua) (CF $?CF))
  (oav (object quartiere) (attribute nome) (value ?qua)
   (CF ?CFqua&~:(member$ ?CFqua $?CF)))
  =>
  (modify ?a (CF (create$ $?CF ?CFqua))))


;; EXTRA
(defrule assegna-CF-extra
 (richiesta (appartamento ?app))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (CF $?CF))
 (oav (object appartamento) (attribute extra) (value ?id)
  (CF ?CFextra&~:(member$ ?CFextra $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFextra))))

;; NUMERO STANZE
(defrule assegna-CF-nstanze
 (richiesta (appartamento ?app))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (nstanze ?stanze) (CF $?CF))
 (oav (object appartamento) (attribute nstanze)
  (value ?st1&:(> ?stanze ?st1) ?st2&:(<= ?stanze ?st2))
  (CF ?CFstanze&~:(member$ ?CFstanze $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFstanze))))

;; NUMERO SERVIZI
(defrule assegna-CF-nservizi
 (richiesta (appartamento ?app))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (nservizi ?servizi) (CF $?CF))
 (oav (object appartamento) (attribute nservizi)
  (value ?servizi) (CF ?CFservizi&~:(member$ ?CFservizi $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFservizi))))

;; BOX AUTO
(defrule assegna-CF-box-auto
 (richiesta (appartamento ?app)
  (vincolo-box_auto ?tipo & hard|soft))
 (appartamento (id ?app) (box_auto ?nbox))
 ?a <- (appartamento (id ?id&:(neq ?id ?app))
     (box_auto ?box) (CF $?CF))
 (oav (object appartamento) (attribute box_auto) (value ?box)
  (CF ?CFbox&~:(member$ ?CFbox $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFbox))))

;; TERRAZZO
(defrule assegna-CF-terrazzo
 (richiesta (appartamento ?app)
  (vincolo-terrazzo ?vincolo & hard|soft))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (terrazzo ?terrazzo) (CF $?CF))
 (oav (object appartamento) (attribute terrazzo) (value ?terrazzo)
  (CF ?CFterrazzo&~:(member$ ?CFterrazzo $?CF)))
 =>
 (modify ?a (CF (create$ $?CF ?CFterrazzo))))

;; REGOLE AGGIUNTIVE
(defrule terrazzo-con-meno-stanze
 (richiesta (appartamento ?app)
  (vincolo-terrazzo ?vincolo & hard|soft))
 (appartamento (id ?app) (nstanze ?dstanze))
 (appartamento (id ?id&:(neq ?id ?app)) (terrazzo si)
  (nstanze ?stanze&:(and(< ?stanze ?dstanze)(> ?stanze 0))) (CF $?CF))
 (oav (object appartamento) (attribute terrazzo) (value si)
  (CF ?CFterrazzo&:(> ?CFterrazzo 0.0)))
 (oav (object appartamento) (attribute nstanze)
  (value ?st1&:(> ?stanze ?st1) ?st2&:(<= ?stanze ?st2))
  (CF ?CFstanze&:(> ?CFstanze 0.0)))
 =>
 (assert (oav (object appartamento) (attribute id) (value ?id)
         (CF (* 0.6 (min ?CFterrazzo ?CFstanze))))))

(defrule piano-diverso-mq-prezzo-buoni
 (richiesta (appartamento ?app)
  (vincolo-piano ?vincolo & hard|soft))
 (appartamento (id ?app) (piano ?dpiano))
 (appartamento (id ?id&:(neq ?id ?app))
  (piano ?piano&:(<> ?piano ?dpiano)) (prezzo ?prezzo) (mq ?mq) (CF $?CF))
 (oav (object appartamento) (attribute prezzo)
  (value ?p1&:(<= ?p1 ?prezzo) ?p2&:(< ?prezzo ?p2))
  (CF ?CFprezzo&:(>= ?CFprezzo 0.25)))
 (oav (object appartamento) (attribute mq)
  (value ?mq1X&:(<= ?mq1X ?mq) ?mq2X&:(< ?mq ?mq2X))
  (CF ?CFmq&:(>= ?CFmq 0.25)))
 (oav (object appartamento) (attribute piano)
  (value ?piano) (CF ?CFpiano&:(and (>= ?CFpiano 0.0) (< ?CFpiano 1.0))))
 =>
 (assert (oav (object appartamento) (attribute id) (value ?id)
         (CF (* 0.9 (min ?CFmq ?CFprezzo ?CFpiano))))))

(defrule zona-diversa-prezzo-buono
 (richiesta (appartamento ?app)
  (vincolo-zona ?vincolo & hard|soft))
 (appartamento (id ?app) (zona ?dzona))
 (appartamento (id ?id&:(neq ?id ?app))
  (zona ?zona&:(neq ?zona ?dzona)) (prezzo ?prezzo) (CF $?CF))
 (oav (object appartamento) (attribute zona) (value ?zona)
  (CF ?CFzona&:(> ?CFzona 0.0)))
 (oav (object appartamento) (attribute prezzo)
  (value ?p1&:(<= ?p1 ?prezzo) ?p2&:(< ?prezzo ?p2))
  (CF ?CFprezzo&:(>= ?CFprezzo 0.25)))
 =>
 (assert (oav (object appartamento) (attribute id) (value ?id)
         (CF (* 0.4 (min ?CFzona ?CFprezzo))))))

(deffunction calcolo-penalita (?mq ?stanze ?servizi ?piano ?citta ?zona
                               ?quartiere ?box ?terrazzo)
  (bind ?penalita 0.0)
  (if (= 0 ?mq) then (bind ?penalita (- ?penalita 0.05)))
  (if (= 0 ?stanze) then (bind ?penalita (- ?penalita 0.05)))
  (if (= 0 ?servizi) then (bind ?penalita (- ?penalita 0.05)))
  (if (= -20 ?piano) then (bind ?penalita (- ?penalita 0.05)))
  (if (eq nil ?citta) then (bind ?penalita (- ?penalita 0.05)))
  (if (eq noanswer ?zona) then (bind ?penalita (- ?penalita 0.05)))
  (if (eq nil ?quartiere) then (bind ?penalita (- ?penalita 0.05)))
  (if (= -1 ?box) then (bind ?penalita (- ?penalita 0.05)))
  (if (eq unk ?terrazzo) then (bind ?penalita (- ?penalita 0.05)))
  ?penalita
)

(defrule valori-sconosciuti
 (richiesta (appartamento ?app))
 (appartamento (id ?id&:(neq ?id ?app)) (mq ?mq) (nstanze ?stanze)
  (nservizi ?servizi) (piano ?piano) (citta ?citta) (zona ?zona)
  (quartiere ?quartiere)(box_auto ?box) (terrazzo ?terrazzo))
 =>
 (bind ?penalita (calcolo-penalita ?mq ?stanze ?servizi ?piano ?citta ?zona
                  ?quartiere ?box ?terrazzo))
 (if (< ?penalita 0.0) then
  (assert (oav (object appartamento) (attribute id)
           (value ?id) (CF ?penalita)))))

(defrule servizi-richiesti
 (richiesta (appartamento ?app))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (quartiere ?quartiere)
  (CF $?CF))
 (quartiere (nome ?quartiere) (servizi $?servizi))
 (oav (object appartamento)
  (attribute servizio ?servizio&:(member$ ?servizio $?servizi))
  (value si) (CF ?CFservizio&~:(member$ ?CFservizio $?CF)))
=>
  (set-fact-duplication TRUE)
  (assert (oav (object appartamento) (attribute id)
           (value ?id) (CF ?CFservizio)))
  (set-fact-duplication FALSE))

(defrule servizi-richiesti-assenti
 (richiesta (appartamento ?app))
 ?a <- (appartamento (id ?id&:(neq ?id ?app)) (quartiere ?quartiere)
  (CF $?CF))
 (quartiere (nome ?quartiere) (servizi $?servizi))
 (oav (object appartamento)
  (attribute servizio ?servizio&~:(member$ ?servizio $?servizi))
  (value si) (CF ?CFservizio&~:(member$ (* -0.5 ?CFservizio) $?CF)))
=>
  (set-fact-duplication TRUE)
  (assert (oav (object appartamento) (attribute id)
           (value ?id) (CF (* -1.0 ?CFservizio))))
  (set-fact-duplication FALSE))

(defrule extra-richiesti
 (richiesta (appartamento ?app))
 (appartamento (id ?id&:(neq ?id ?app)) (CF $?CF) (extra $?extras))
 (oav (object appartamento)
  (attribute extra ?extra&:(member$ ?extra $?extras))
  (value si) (CF ?CFextra&~:(member$ ?CFextra $?CF)))
=>
  (set-fact-duplication TRUE)
  (assert (oav (object appartamento) (attribute id)
           (value ?id) (CF ?CFextra)))
  (set-fact-duplication FALSE))

(defrule extra-richiesti-assenti
 (richiesta (appartamento ?app))
 (appartamento (id ?id&:(neq ?id ?app)) (CF $?CF) (extra $?extras))
 (oav (object appartamento)
  (attribute extra ?extra&~:(member$ ?extra $?extras))
  (value si) (CF ?CFextra&~:(member$ (* -0.5 ?CFextra) $?CF)))
=>
  (set-fact-duplication TRUE)
  (assert (oav (object appartamento) (attribute id)
           (value ?id) (CF (* -1.0 ?CFextra))))
  (set-fact-duplication FALSE))
