;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule CONSTRAINT
 (import OAV deftemplate oav)
 (import DOMAIN deftemplate ?ALL)
)

;; SERVIZI SPORTIVI
(defrule cliente-sportivo-1
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ palestra $?servizi)))
 (cliente (id ?cl) (sportivo si))
 =>
 (modify ?r (servizi (create$ $?servizi palestra)))
)

(defrule cliente-sportivo-2
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ piscina $?servizi)))
 (cliente (id ?cl) (sportivo si))
 =>
 (modify ?r (servizi (create$ $?servizi piscina)))
)

;; SERVIZI PENDOLARI
(defrule cliente-pendolare
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ treno $?servizi)))
 (cliente (id ?cl) (pendolare si))
 =>
 (modify ?r (servizi (create$ $?servizi treno)))
)

;; SERVIZI STUDENTI UNIVERSITARI
(defrule cliente-studente
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ universita $?servizi)))
 (cliente (id ?cl) (studente si))
 =>
 (modify ?r (servizi (create$ $?servizi universita)))
)

;; SERVIZI CLIENTI CON FIGLI
(defrule cliente-con-figli-1
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ asilo $?servizi)))
 (cliente (id ?cl) (figli ?f&:(> ?f 0)))
 =>
 (modify ?r (servizi (create$ $?servizi asilo)))
)

(defrule cliente-con-figli-2
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ scuola $?servizi)))
 (cliente (id ?cl) (figli ?f&:(> ?f 0)))
 =>
 (modify ?r (servizi (create$ $?servizi scuola)))
)

;; SERVIZI CLIENTI MEZZI PUBBLICI
(defrule cliente-mezzi-pubblici-1
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ metro $?servizi)))
 (cliente (id ?cl) (mezzi si))
 =>
 (modify ?r (servizi (create$ $?servizi metro)))
)

(defrule cliente-mezzi-pubblici-2
 ?r <- (richiesta (cliente ?cl)
     (servizi $?servizi&~:(member$ autobus $?servizi)))
 (cliente (id ?cl) (mezzi si))
 =>
 (modify ?r (servizi (create$ $?servizi autobus)))
)

;; CLIENTE OVER 50
(defrule cliente-ascensore
  (richiesta (cliente ?cl1) (appartamento ?app))
  (cliente (id ?cl1) (eta ?eta&:(>= ?eta 50)))
  ?ar <- (appartamento (id ?app) (extra $?extra&~:(member$ ascensore $?extra)))
  =>
  (modify ?ar (extra (create$ $?extra ascensore))))
