;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule OAV (export deftemplate oav))

  (deftemplate oav
   (multislot object (type SYMBOL))
   (multislot attribute (type SYMBOL))
   (multislot value)
   (slot CF (type FLOAT) (range -1.0 +1.0) (default 1.0)))

  (defrule combine-CF-positive
   (declare (auto-focus TRUE))
   ?fact1 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF1&:(>= ?CF1 0.0)))
   ?fact2 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF2&:(>= ?CF2 0.0)))
   (test (neq ?fact1 ?fact2))
   =>
   (retract ?fact1)
   (bind ?CF3 (- (+ ?CF1 ?CF2) (* ?CF1 ?CF2)))
   (modify ?fact2 (CF ?CF3)))

  (defrule combine-CF-negative
   (declare (auto-focus TRUE))
   ?fact1 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF1&:(< ?CF1 0.0)))
   ?fact2 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF2&:(< ?CF2 0.0)))
   (test (neq ?fact1 ?fact2))
   =>
   (retract ?fact1)
   (bind ?CF3 (+ (+ ?CF1 ?CF2) (* ?CF1 ?CF2)))
   (modify ?fact2 (CF ?CF3)))

  (defrule combine-CF-different
   (declare (auto-focus TRUE))
   ?fact1 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF1&:(<> ?CF1 1.0 -1.0)))
   ?fact2 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF ?CF2&:(<= (* ?CF1 ?CF2) 0.0)))
   (test (neq ?fact1 ?fact2))
   =>
   (retract ?fact1)
   (bind ?CF3 (/ (+ ?CF1 ?CF2)
              (- 1 (min (abs ?CF1) (abs ?CF2)))))
   (modify ?fact2 (CF ?CF3)))

  (defrule combine-CF-different-extreme
   (declare (auto-focus TRUE))
   ?fact1 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF 1.0))
   ?fact2 <- (oav (object $?o)
     (attribute $?a)
     (value $?v)
     (CF -1.0))
   (test (neq ?fact1 ?fact2))
   =>
   (retract ?fact1)
   (modify ?fact2 (CF 0.0)))
