;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule PROPOSAL
 (import OAV deftemplate oav)
 (import DOMAIN deftemplate ?ALL)
)

(deffunction multimin ($?list)
 (bind ?min 1.0)
 (foreach ?el $?list
  (bind ?min (min ?min ?el))
 ) ?min
)

(defrule genera-proposte
 (richiesta (appartamento ?app)
  (vincolo-nstanze ?vnstanze)
  (vincolo-nservizi ?vnservizi)
  (vincolo-piano ?vpiano)
  (vincolo-zona  ?vzona)
  (vincolo-quartiere ?vquartiere)
  (vincolo-box_auto ?vbox)
  (vincolo-terrazzo ?vterrazzo)
 )
 ?a <- (appartamento (id ?id&:(neq ?id ?app))
  (CF $?CF&:(> (length$ $?CF) 0)))
 =>
 (assert (oav (object appartamento) (attribute id) (value ?id)
         (CF (multimin $?CF))))
 (modify ?a (CF (create$)))
)

(defrule stampa-risultati (declare (salience -100))
 ?app <- (oav (object appartamento) (attribute id) (value ?id)
     (CF ?CF&:(> ?CF -0.2)))
 (not (oav (object appartamento) (attribute id) (CF ?CFZ&:(> ?CFZ ?CF))))
 (appartamento (id ?id)
  (mq ?mq)
  (nstanze ?nstanze)
  (nservizi ?nservizi)
  (piano ?piano)
  (citta ?citta)
  (zona ?zona)
  (quartiere ?quartiere)
  (box_auto ?box_auto)
  (terrazzo ?terrazzo)
  (extra $?extras)
  (prezzo ?prezzo))
 (quartiere (nome ?quartiere) (servizi $?servizi))
 =>
 (printout t "Appartamento-" ?id " CF:" ?CF crlf)
 (if (> ?mq 0) then (printout t tab "- " ?mq " metri quadrati" crlf))
 (if (> ?prezzo 1) then (printout t tab "- " ?prezzo " euro" crlf))
 (if (> ?nstanze 0) then (printout t tab "- " ?nstanze " stanze" crlf))
 (if (> ?nservizi 0) then (printout t tab "- " ?nservizi " bagni" crlf))
 (foreach ?extra $?extras(printout t tab tab "- " ?extra crlf))
 (printout t tab "- " ?piano "° piano" crlf)
 (if (neq ?citta nil) then (printout t tab "- Situata a " ?citta crlf))
 (if (neq ?zona noanswer) then (printout t tab "- In zona " ?zona crlf))
 (if (neq ?quartiere nil) then (printout t tab "- Nel quartiere " ?quartiere crlf))
 (foreach ?servizio $?servizi(printout t tab tab "- " ?servizio crlf))
 (if (> ?box_auto 0) then (printout t tab "- Con " ?box_auto " box auto" crlf))
 (if (eq ?terrazzo si) then (printout t tab "- Con terrazzo" crlf))
 (retract ?app)
)

(defrule resetta-valori (declare (salience -100))
 ?app <- (oav (object appartamento) (attribute id) (value ?id)
     (CF ?CF&:(<= ?CF -0.2)))
 =>
 (retract ?app))
