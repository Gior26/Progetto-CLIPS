;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.
(defmodule MAIN
 (import QUESTIONS deftemplate domanda)
 (import OAV deftemplate oav)
)

(deffacts MAIN::control-information
 (phase-sequence QUESTIONS CONSTRAINT CFGENERATION RULES PROPOSAL))

(defrule MAIN::change-phase
 ?list <- (phase-sequence ?next $?rest)
 =>
 (focus ?next)
 (retract ?list)
 (assert (phase-sequence ?rest ?next)))

(defrule reset-saltate (declare (salience 500))
 ?d <- (domanda (saltata TRUE))
 =>
 (modify ?d (saltata FALSE)))

(defrule reset-CF-quartieri (declare (salience 500))
 (phase-sequence QUESTIONS $?rest)
 ?oav <- (oav (object quartiere))
 =>
 (retract ?oav))

(defrule reset-CF-nstanze (declare (salience 500))
 (phase-sequence QUESTIONS $?rest)
 ?oav <- (oav (object appartamento) (attribute nstanze))
 =>
 (retract ?oav))

(defrule reset-CF-extra (declare (salience 500))
 (phase-sequence QUESTIONS $?rest)
 ?oav <- (oav (object appartamento) (attribute extra))
 =>
 (retract ?oav))
